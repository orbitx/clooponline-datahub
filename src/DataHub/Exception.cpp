#include "Exception.h"

#include <sstream>

clooponline::Exception::Exception(const clooponline::LogClass::LogClass& log_class, const std::string& msg)
{
    mLogClass = log_class;
    mMessage = msg;

    std::stringstream ss;
    ss << "(" << LogClass::toString(mLogClass) << ") " << mMessage;
    mCache = ss.str();
}

const char* clooponline::Exception::what() const throw()
{
    return mCache.c_str();
}

