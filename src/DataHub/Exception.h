#ifndef CLOOP_DH_EXCEPTION
#define CLOOP_DH_EXCEPTION

#include "Log.h"

#include <exception>
#include <string>


namespace clooponline
{
    class Exception : public std::exception
    {
    public:
        Exception(const LogClass::LogClass & log_class, const std::string & msg);

        const char * what() const throw();

    private:
        LogClass::LogClass mLogClass;
        std::string mMessage;
        std::string mCache;
    };

};


#endif // CLOOP_DH_EXCEPTION
