#include "RedisInterface.h"
#include "DataHub.h"
#include "Log.h"


clooponline::DataHub::DataHub()
{

}

clooponline::DataHub::~DataHub()
{

}

void clooponline::DataHub::operator=(const clooponline::DataHub& other)
{
}

clooponline::DataHub* clooponline::DataHub::connectToRedis(const std::string & host, const int & port)
{
    RedisInterface * redis_interface = new RedisInterface();
    if (redis_interface->connect(host, port))
      return redis_interface;

    delete redis_interface;
    return NULL;
}

clooponline::DataHub* clooponline::DataHub::connectToSharedMemory(const std::string& shmName, const int& shmSize)
{
    Logger::wtf(LogClass::DataHub, "Boost SHM is not supported");
    return NULL;
}
