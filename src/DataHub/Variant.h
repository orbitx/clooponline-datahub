#pragma once

#include <string>
#include <unordered_map>
#include <list>
#include <sstream>
#include <vector>

using std::stringstream;
#include <iostream>

namespace clooponline
{
    enum VariantType
    {
        Boolean = 1,
        Integer = 2,
        Long = 3,
        Double = 4,
        String = 5,
        Map = 6
    };

    class Variant;
    typedef std::unordered_map<std::string, Variant> VariantMap;

    class Variant
    {
    public:
        typedef std::vector<Variant> Array;

    public:
        Variant(const Variant & other);
        Variant(Variant && other);
        Variant & operator = (Variant && other);
        ~Variant();

        Variant(const bool & value);
        Variant(const int & value);
        Variant(const long & value);
        Variant(const double & value);
        Variant(const char * value);
        Variant(const std::string & value);
        Variant(const VariantMap & map);

        VariantMap exportToMap(const VariantType & valueType);
        const std::list<std::string> & getMapElements() const;

        VariantType getType() const;
        bool isInitialized() const;
        std::string toString() const;

        operator bool() const;
        operator int() const;
        operator long() const;
        operator double() const;
        operator std::string() const;

    public:
        static Variant parse(const VariantType & type, const char * value, const size_t & size);
        static Variant parse(const VariantType & type, const std::string & value);
        static Variant constructMapFromKeysAndValues(const std::list<std::string> & entries);
        static Variant Nothing;

    private:
        union VariantField
        {
            bool bValue;
            int iValue;
            long lValue;
            double dValue;
            void * objValue;
        };

    private:
        bool mInitialized;
        VariantType mType;
        VariantField mValue;

        Variant();
    };
};
