#include "Log.h"
#include <iostream>
#include <map>
#include <boost/assign.hpp>
#include <stdarg.h>


using std::cout;
using std::cerr;
using std::endl;


namespace clooponline
{
    /* Static member definition for Logger::mLogFilterSeverity */
    LogSeverity::LogSeverity Logger::mLogFilterSeverity = LogSeverity::All;

    /* Static member definition for Logger::mLogFilterTag */
    std::string Logger::mLogFilterTag = "";

    /* Static member definition for singleton Logger::mInstance */
    template<>
    Logger * Singleton<Logger>::mInstance = NULL;
}

const std::string & clooponline::LogClass::toString(const clooponline::LogClass::LogClass& logClass)
{
    static std::map<LogClass, std::string> table =
        boost::assign::map_list_of
            (Data, "Data")
            (DataHub, "DataHub")
            (BoostSHM, "BoostSHM")
            (Redis, "Redis")
            (Variant, "Variant")
            (CLI, "CLI")
            (Test, "Test");

    return table[logClass];
}


const std::string & clooponline::LogSeverity::toString(const clooponline::LogSeverity::LogSeverity& logSeverity)
{
    static std::map<LogSeverity, std::string> table =
        boost::assign::map_list_of
            (All, "log")
            (Debug, "debug")
            (Info, "info")
            (Warning, "warn")
            (Error, "error")
            (WTF, "wtf");

    return table[logSeverity];
}


clooponline::Logger::Logger()
{

}

void clooponline::Logger::log(const clooponline::LogSeverity::LogSeverity& logSeverity, const std::string & logTag, const std::string & logMsg)
{
    if(logSeverity < mLogFilterSeverity)
        return;
    else if(mLogFilterTag.length() != 0 && mLogFilterTag.find(logMsg) == mLogFilterTag.npos)
        return;

    std::ostream & out = logSeverity >= LogSeverity::Error ? cerr : cout;
    out << LogSeverity::toString(logSeverity) << "/" << logTag << ": " << logMsg << endl;
}

void clooponline::Logger::filter(const clooponline::LogSeverity::LogSeverity& logSeverity, const std::string & logTag)
{
    mLogFilterSeverity = logSeverity;
    mLogFilterTag = logTag;
}

void clooponline::Logger::debug(const clooponline::LogClass::LogClass& logClass, const std::string & logMsg)
{
    getSingleton()->log(LogSeverity::Debug, LogClass::toString(logClass), logMsg);
}

void clooponline::Logger::info(const clooponline::LogClass::LogClass& logClass, const std::string & logMsg)
{
    getSingleton()->log(LogSeverity::Info, LogClass::toString(logClass), logMsg);
}

void clooponline::Logger::warning(const clooponline::LogClass::LogClass& logClass, const std::string & logMsg)
{
    getSingleton()->log(LogSeverity::Warning, LogClass::toString(logClass), logMsg);
}

void clooponline::Logger::error(const clooponline::LogClass::LogClass& logClass, const std::string & logMsg)
{
    getSingleton()->log(LogSeverity::Error, LogClass::toString(logClass), logMsg);
}

void clooponline::Logger::wtf(const clooponline::LogClass::LogClass& logClass, const std::string & logMsg)
{
    getSingleton()->log(LogSeverity::WTF, LogClass::toString(logClass), logMsg);
}

std::string clooponline::Logger::format(const char* strFormat, ...)
{
    static char dest[1024 * 16];
    va_list argptr;
    va_start(argptr, strFormat);
    vsprintf(dest, strFormat, argptr);
    va_end(argptr);
    return dest;
}

clooponline::Logger* clooponline::Logger::getSingleton()
{
    if(!mInstance)
        mInstance = new Logger;

    return mInstance;
}

void clooponline::Logger::freeSingleton()
{
    if(mInstance)
        delete mInstance;

    mInstance = NULL;
}
