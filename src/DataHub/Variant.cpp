#include "Variant.h"
#include "Log.h"

#define CONV_STRING(x) (*static_cast<std::string*>(x))
#define CONV_STD_LIST(x) (*static_cast<std::list<std::string>*>(x))

namespace clooponline
{
    /* Static Variant member: Nothing */
    Variant Variant::Nothing;
}

clooponline::Variant::Variant()
{
    mInitialized = false;
}

clooponline::Variant::Variant(const Variant & other)
{
    mInitialized = other.mInitialized;
    mType = other.mType;

    if(mType == String)
        mValue.objValue =
            other.mValue.objValue == NULL
                ? NULL
                : new std::string(CONV_STRING(other.mValue.objValue));
    else if(mType == Map)
        mValue.objValue =
            other.mValue.objValue == NULL
                ? NULL
                : new std::list<std::string>(CONV_STD_LIST(other.mValue.objValue));
    else
        mValue = other.mValue;
}

clooponline::Variant::Variant(Variant && other)
{
    // can not be made into one function with operator = (&&)
    // very sad :'(

    mInitialized = other.mInitialized;
    mType = other.mType;
    mValue = other.mValue;

    if(mType == String || mType == Map)
        other.mValue.objValue = NULL;
}

clooponline::Variant & clooponline::Variant::operator = (Variant && other)
{
    // can not be made into one function with Constructor(&&)
    // very sad :'(

    mInitialized = other.mInitialized;
    mType = other.mType;
    mValue = other.mValue;

    if(mType == String || mType == Map)
        other.mValue.objValue = NULL;

    return *this;
}

clooponline::Variant::~Variant()
{
    if(mType == String && mValue.objValue != NULL)
        delete &CONV_STRING(mValue.objValue);
    else if(mType == Map && mValue.objValue != NULL)
        delete &CONV_STD_LIST(mValue.objValue);
}

clooponline::Variant::Variant(const bool & value)
{
    mType = Boolean;
    mValue.bValue = value;
    mInitialized = true;
}

clooponline::Variant::Variant(const int & value)
{
    mType = Integer;
    mValue.iValue = value;
    mInitialized = true;
}

clooponline::Variant::Variant(const long & value)
{
    mType = Long;
    mValue.lValue = value;
    mInitialized = true;
}

clooponline::Variant::Variant(const double & value)
{
    mType = Double;
    mValue.dValue = value;
    mInitialized = true;
}

clooponline::Variant::Variant(const char* value)
{
    mType = String;
    mValue.objValue = new std::string(value);
    mInitialized = true;
}

clooponline::Variant::Variant(const std::string & value)
{
    mType = String;
    mValue.objValue = new std::string(value);
    mInitialized = true;
}

clooponline::Variant::Variant(const VariantMap & map)
{
    mType = clooponline::Map;
    mValue.objValue = new std::list<std::string>;
    mInitialized = true;

    auto & list = CONV_STD_LIST(mValue.objValue);
    for(const auto & pair : map)
    {
        list.push_back(pair.first);
        list.push_back(pair.second.toString());
    }
}

clooponline::VariantMap clooponline::Variant::exportToMap(const VariantType & valueType)
{
    VariantMap result;

    if(!mInitialized || !mValue.objValue)
        return result;

    const auto list = CONV_STD_LIST(mValue.objValue);
    for(auto it = list.begin(); it != list.end(); ++it)
    {
        const auto & pair = result.find(*it);
        std::string key = *it;
        std::string value = *++it;
        if(pair == result.end())
            result.insert(std::make_pair(key, value));
        else
            pair->second = Variant(*++it);
    }

    return result;
}

const std::list<std::string> & clooponline::Variant::getMapElements() const
{
    return CONV_STD_LIST(mValue.objValue);
}

clooponline::VariantType clooponline::Variant::getType() const
{
    return mType;
}

bool clooponline::Variant::isInitialized() const
{
    return mInitialized;
}


std::string clooponline::Variant::toString() const
{
    stringstream ss;

    if(mInitialized)
        switch(mType)
        {
            case Boolean:
                ss << (mValue.bValue ? "true" : "false");
                break;

            case Integer:
                ss << mValue.iValue;
                break;

            case Long:
                ss << mValue.lValue;
                break;

            case Double:
                ss << mValue.dValue;
                break;

            case String:
                ss << CONV_STRING(mValue.objValue);
                break;

            default:
                ss << "?";
                break;
        }
    else
        ss << "Nothing";

    return ss.str();
}

clooponline::Variant::operator bool() const
{
    return mValue.bValue;
}

clooponline::Variant::operator int() const
{
    return mValue.iValue;
}

clooponline::Variant::operator long() const
{
    return mValue.lValue;
}

clooponline::Variant::operator double() const
{
    return mValue.dValue;
}

clooponline::Variant::operator std::string() const
{
    return CONV_STRING(mValue.objValue);
}

clooponline::Variant clooponline::Variant::parse(const clooponline::VariantType& type, const char* value, const size_t& size)
{
    switch(type)
    {
        case Boolean:
            return value[0] == 't' ? Variant(true) : Variant(false);

        case Integer:
            return Variant(std::atoi(value));

        case Long:
            return Variant(std::atol(value));

        case Double:
            return Variant(std::atof(value));

        case String:
            return Variant(std::string(value, static_cast<size_t>(size)));

        default:
            Logger::error(LogClass::Variant, Logger::format("Unsupported value type %d", type));
            return Variant::Nothing;
    }
}

clooponline::Variant clooponline::Variant::parse(const clooponline::VariantType& type, const std::string& value)
{
    return parse(type, value.c_str(), value.length());
}

clooponline::Variant clooponline::Variant::constructMapFromKeysAndValues(const std::list< std::string >& entries)
{
    Variant v;
    v.mType = clooponline::Map;
    v.mValue.objValue = new std::list<std::string>(entries);
    v.mInitialized = true;
    return v;
}

