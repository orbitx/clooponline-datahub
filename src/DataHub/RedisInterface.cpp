#include "RedisInterface.h"
#include "Log.h"

#include "Exception.h"
#include <cstdlib>
#include <vector>
#include <list>
#include <sstream>


#include <iostream>
using std::cerr;
using std::cout;
using std::endl;

#define RedisString  REDIS_REPLY_STRING
#define RedisArray   REDIS_REPLY_ARRAY
#define RedisInteger REDIS_REPLY_INTEGER
#define RedisNil     REDIS_REPLY_NIL
#define RedisStatus  REDIS_REPLY_STATUS
#define RedisError   REDIS_REPLY_ERROR


std::set<int> clooponline::RedisInterface::mReadReplyTypes
    {RedisString, RedisArray, RedisInteger};

std::set<int> clooponline::RedisInterface::mWriteReplyTypes
    {RedisInteger, RedisStatus};


clooponline::RedisInterface::RedisInterface()
{

}

clooponline::RedisInterface::~RedisInterface()
{
    if(mIsConnected)
        redisFree(mRedis);
}

bool clooponline::RedisInterface::connect(const std::string& host, const int& port)
{
    mRedis = redisConnect(host.c_str(), port);
    if(mRedis && !mRedis->err)
        return (mIsConnected = true);

    Logger::error(
        LogClass::Redis,
        Logger::format("Could not connect to '%s:%d' with error '%s'", host.c_str(), port, mRedis->errstr)
    );

    redisFree(mRedis);
    return (mIsConnected = false);
}

clooponline::Variant clooponline::RedisInterface::read(const VariantType & varType, const std::string& name, const Variant& defaultValue)
{
    return
        varType != Map ?
        executeRedisCall(varType, {"get", name}, mReadReplyTypes, defaultValue) :
        executeRedisCall(varType, {"HGETALL", name}, mReadReplyTypes, defaultValue);
}

clooponline::Variant clooponline::RedisInterface::readMapEntry(const VariantType& varType, const std::string& varName, const std::string& varKey, const Variant& defaultValue)
{
    return executeRedisCall(varType, {"HGET", varName, varKey}, mReadReplyTypes, defaultValue);
}

bool clooponline::RedisInterface::write(const VariantType& varType, const std::string& varName, const Variant& value)
{
    switch(varType)
    {
        case Boolean:
        case Integer:
        case Long:
        case Double:
        case String:
            return
                executeRedisCall(Boolean, {"SET", varName, value.toString()}, mWriteReplyTypes)
                    .isInitialized();

        case Map:
        {
            std::vector<std::string> cmdParts;
            cmdParts.push_back("HMSET");
            cmdParts.push_back(varName);
            for(const auto & element : value.getMapElements())
                cmdParts.push_back(element);

            return
                executeRedisCall(Map, cmdParts, mWriteReplyTypes)
                    .isInitialized();
        }

        default:
            return false;
    }
}

bool clooponline::RedisInterface::writeMapEntry(const VariantType& varType, const std::string& varName, const std::string& varKey, const Variant& value)
{
    return
        executeRedisCall(varType, {"HSET", varName, varKey, value.toString()}, mWriteReplyTypes)
            .isInitialized();
}

bool clooponline::RedisInterface::drop(const VariantType& varType, const std::string& varName)
{
    return
        executeRedisCall(varType, {"DEL", varName}, mWriteReplyTypes)
            .isInitialized();
}

bool clooponline::RedisInterface::dropMapEntry(const VariantType& varType, const std::string& varName, const std::string& varKey)
{
    return
        executeRedisCall(varType, {"HDEL", varName, varKey}, mWriteReplyTypes)
            .isInitialized();
}

bool clooponline::RedisInterface::check(const VariantType& varType, const std::string& varName)
{
    long result = executeRedisCall(Integer, {"EXISTS", varName}, mWriteReplyTypes);
    return result != 0;
}

bool clooponline::RedisInterface::checkMapEntry(const VariantType& varType, const std::string& varName, const std::string& varKey)
{
    long result = executeRedisCall(Integer, {"HEXISTS", varName, varKey}, mWriteReplyTypes);
    return result != 0;
}

clooponline::Variant::Array clooponline::RedisInterface::list(const std::string& pattern)
{
    return Variant::Array();
}

clooponline::Variant clooponline::RedisInterface::parseRedisReply(const VariantType& type, redisReply* reply)
{
    if(type == Map)
    {
        std::list<std::string> values;
        for (size_t i = 0; i < reply->elements; ++i)
        {
            redisReply *r = *(reply->element + i);
            values.push_back(std::string(r->str, r->len));
        }

        return Variant::constructMapFromKeysAndValues(values);
    }
    else if(reply->type == RedisInteger)
        return Variant((long)(reply->integer));
    else if(reply->type == RedisStatus)
        return Variant(reply->str);
    else
        return Variant::parse(type, reply->str, reply->len);
}

std::string clooponline::RedisInterface::generateHumanReadableCommand(const std::vector<std::string >& parts)
{
    std::stringstream cmd;
    for (const auto & value : parts)
        cmd << value << " ";

    std::string result = cmd.str();
    result.erase(result.length() - 1);
    return result;
}

clooponline::Variant clooponline::RedisInterface::executeRedisCall(
    const VariantType & replyType,
    const std::vector<std::string >& parts,
    const std::set<int> & acceptedReplies,
    const Variant & defaultResult)
{
    std::vector<const char *> argv;
    std::vector<size_t> argvlen;

    for(const auto & part : parts)
    {
        argv.push_back(part.c_str());
        argvlen.push_back(part.length());
    }

    redisReply * reply = (redisReply*)redisCommandArgv(mRedis, argv.size(), &(argv[0]), &(argvlen[0]));
    if(!reply)
    {
        Logger::error(LogClass::Redis, "No reply returned");
        return defaultResult;
    }

    Variant result = defaultResult;
    if(reply->type == RedisError)
        Logger::error(
            LogClass::Redis,
            Logger::format(
                "Reply for `%s` returned error `%s`",
                generateHumanReadableCommand(parts).c_str(),
                reply->str
            )
        );

    else if(acceptedReplies.find(reply->type) == acceptedReplies.end())
        Logger::error(
            LogClass::Redis,
            Logger::format(
                "Command did not return any expected reply type: '%s' got reply type '%d'",
                generateHumanReadableCommand(parts).c_str(),
                reply->type
            )
        );

    else
        result = parseRedisReply(replyType, (redisReply*)reply);

    freeReplyObject(reply);
    return result;
}
