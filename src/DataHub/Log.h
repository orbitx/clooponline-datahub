#pragma once

#include "Singleton.h"
#include <string>


namespace clooponline
{

    namespace LogClass
    {
        enum LogClass
        {
            Data,
            DataHub,
            BoostSHM,
            Redis,
            CLI,
            Variant,
            Test
        };

        const std::string & toString(const LogClass & logClass);
    };

    namespace LogSeverity
    {
        enum LogSeverity
        {
            All,
            Debug,
            Info,
            Warning,
            Error,
            WTF
        };

        const std::string & toString(const LogSeverity & logSeverity);
    }


    class Logger : public Singleton<Logger>
    {
    public:
        void log(
            const LogSeverity::LogSeverity & logSeverity,
            const std::string & logTag,
            const std::string & logMsg
        );

        static void filter(
            const LogSeverity::LogSeverity & logSeverity,
            const std::string & logTag
        );

        static void debug(const LogClass::LogClass & logClass, const std::string & logMsg);
        static void info(const LogClass::LogClass & logClass, const std::string & logMsg);
        static void warning(const LogClass::LogClass & logClass, const std::string & logMsg);
        static void error(const LogClass::LogClass & logClass, const std::string & logMsg);
        static void wtf(const LogClass::LogClass & logClass, const std::string & logMsg);
        static std::string format(const char * strFormat, ...);

        static Logger * getSingleton();
        static void freeSingleton();

    private:
        Logger();

    private:
        static LogSeverity::LogSeverity mLogFilterSeverity;
        static std::string mLogFilterTag;
    };

}; // end namespace clooponline
