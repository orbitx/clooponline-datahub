#pragma once

#include <hiredis/hiredis.h>
#include <set>

#include "DataHub.h"


namespace clooponline
{

    class RedisInterface : public DataHub
    {
    public:
        RedisInterface();
        virtual ~RedisInterface();

        virtual Variant read(
            const VariantType & varType,
            const std::string & varName,
            const Variant & defaultValue = Variant::Nothing
        );

        virtual Variant readMapEntry(
            const VariantType & varType,
            const std::string & varName,
            const std::string & varKey,
            const Variant & defaultValue = Variant::Nothing
        );

        virtual bool write(
            const VariantType & varType,
            const std::string & varName,
            const Variant & value
        );

        virtual bool writeMapEntry(
            const VariantType & varType,
            const std::string & varName,
            const std::string & varKey,
            const Variant & value
        );

        virtual bool drop(
            const VariantType & varType,
            const std::string & varName
        );

        virtual bool dropMapEntry(
            const VariantType & varType,
            const std::string & varName,
            const std::string & varKey
        );

        virtual bool check(
            const VariantType & varType,
            const std::string & varName
        );

        virtual bool checkMapEntry(
            const VariantType & varType,
            const std::string & varName,
            const std::string & varKey
        );

        virtual Variant::Array list(
            const std::string & pattern = ""
        );

        bool connect(const std::string& host, const int& port);

    private:
        redisContext * mRedis;
        bool mIsConnected;

        static std::set<int> mReadReplyTypes;
        static std::set<int> mWriteReplyTypes;

    private:
        static Variant parseRedisReply(
            const VariantType & type,
            redisReply * reply
        );

        static std::string generateHumanReadableCommand(
            const std::vector<std::string >& parts
        );

        Variant executeRedisCall(
            const VariantType & replyType,
            const std::vector<std::string> & parts,
            const std::set<int> & acceptedReplies,
            const Variant & defaultResult = Variant::Nothing
        );
    };

};
