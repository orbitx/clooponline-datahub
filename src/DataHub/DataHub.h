#pragma once

#include "Variant.h"


namespace clooponline
{
    class DataHub
    {
    public:
        virtual ~DataHub();

        virtual Variant read(
            const VariantType & varType,
            const std::string & varName,
            const Variant & defaultValue = Variant::Nothing
        ) = 0;

        virtual Variant readMapEntry(
            const VariantType & varType,
            const std::string & varName,
            const std::string & varKey,
            const Variant & defaultValue = Variant::Nothing
        ) = 0;

        virtual bool write(
            const VariantType & varType,
            const std::string & varName,
            const Variant & value
        ) = 0;

        virtual bool writeMapEntry(
            const VariantType & varType,
            const std::string & varName,
            const std::string & varKey,
            const Variant & value
        ) = 0;

        virtual bool drop(
            const VariantType & varType,
            const std::string & varName
        ) = 0;

        virtual bool dropMapEntry(
            const VariantType & varType,
            const std::string & varName,
            const std::string & varKey
        ) = 0;

        virtual bool check(
            const VariantType & varType,
            const std::string & varName
        ) = 0;

        virtual bool checkMapEntry(
            const VariantType & varType,
            const std::string & varName,
            const std::string & varKey
        ) = 0;

        virtual Variant::Array list(
            const std::string & pattern = ""
        ) = 0;

        static DataHub * connectToRedis(
            const std::string & host,
            const int & port
        );

        static DataHub * connectToSharedMemory(
            const std::string & shmName,
            const int & shmSize
        );

    protected:
        DataHub();

    private:
        void operator = (const DataHub & other);
    };
};
