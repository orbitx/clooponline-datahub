#include <DataHub.h>
#include <Exception.h>

#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using namespace clooponline;

std::stringstream * stream;
DataHub * hub = NULL;


enum CommandType
{
    Open,
    Close,
    List,
    Get,
    Set,
    Check,
    Drop,
    Nothing,
    Quit
};

CommandType promptForCommand();
void open();
void close();
void printVarHelp();
bool parseType(const std::string & strType, VariantType & output);
void listVars();
void getVar();
void printVarValue(const VariantType & type, const std::string & name);
void setVar();
void checkVar();
void dropVar();


int main()
{
    CommandType cmd = Nothing;
    stream = new std::stringstream();

    while(cmd != Quit)
    {
        if(cmd == Open)
            open();
        else if(cmd == Close)
            close();
        else if(cmd == List)
            listVars();
        else if(cmd == Get)
            getVar();
        else if(cmd == Set)
            setVar();
        else if(cmd == Check)
            checkVar();
        else if(cmd == Drop)
            dropVar();

        cmd = promptForCommand();
    }

    close();
    cout << "bye." << endl;

    return 0;
}


CommandType promptForCommand()
{
    delete stream;

    cout << endl << "DataHub CLI> ";
    char input_buffer[1024];
    cin.getline(input_buffer, 1024);
    stream = new std::stringstream(input_buffer);

    std::string cmd;
    *stream >> cmd;
    if(cmd == "open")
        return Open;
    else if(cmd == "close")
        return Close;
    else if(cmd == "list")
        return List;
    else if(cmd == "set" || cmd == "write")
        return Set;
    else if(cmd == "get" || cmd == "read")
        return Get;
    else if(cmd == "check")
        return Check;
    else if(cmd == "drop")
        return Drop;
    else if(cmd == "quit" || cmd == "exit")
        return Quit;

    cout << endl << "List of commands:" << endl;
    cout << "open type resource [params]    Open requested hub" << endl;
    cout << "close                          Close current hub" << endl;
    cout << "list                           List all variable in the hub" << endl;
    cout << "get name                       Get value for key name" << endl;
    cout << "set name value                 Set value for key name" << endl;
    cout << "quit                           Quit." << endl;

    return Nothing;
}

void open()
{
    if(hub != NULL)
    {
        cout << "A hub is algety open. Close it first." << endl;
        return;
    }

    std::string type;
    *stream >> type;

    if(type == "shm")
    {
        try
        {
            std::string name;
            std::string sizestr;
            int size;
            char modifier;
            if(!stream->eof())
                *stream >> name;
            else
            {
                name = "test";
                cout << "defaulting name to 'test'" << endl;
            }

            if(!stream->eof())
                *stream >> sizestr;
            else
            {
                sizestr = "65k";
                cout << "defaulting size to 65k" << endl;
            }

            modifier = tolower(sizestr[sizestr.length() - 1]);
            *stream << "  "<< sizestr.substr(0, sizestr.length() - 1);
            std::stringstream(sizestr) >> size;

            if(modifier == 'k')
                size = size * 1024;
            else if(modifier == 'm')
                size = size * 1024 * 1024;
            else if(modifier == 'g')
                size = size * 1024 * 1024 * 1024;
            else
                throw clooponline::Exception(LogClass::CLI, std::string("Modifier not valid ") + (char)(toupper(modifier)));

            hub = DataHub::connectToSharedMemory(name, size);
            if(hub)
                cout << "Opened SHM " << name << endl;
            else
                cout << "Open fialed for SHM by " << name << endl;
        }
        catch(clooponline::Exception & ex)
        {
            cout << ex.what() << endl;
        }
        catch(std::exception & ex)
        {
            cout << "Error: could not open shared memory, check usage and parameters with help." << endl;
            cout << "detail: " << ex.what() << endl;
            hub = NULL;
        }

    }
    else if(type == "redis")
    {
        try
        {
            std::string host;
            int port;
            if(!stream->eof())
                *stream >> host;
            else
            {
                host = "localhost";
                cout << "defaulting host to 'localhost'" << endl;
            }

            if(!stream->eof())
                *stream >> port;
            else
            {
                port = 6379;
                cout << "defaulting port to 6379" << endl;
            }

            hub = DataHub::connectToRedis(host, port);
            if(hub)
                cout << "Opened redis at " << host << ":" << port << endl;
            else
                cout << "Open failed for redis at " << host << ":" << port << endl;
        }
        catch(clooponline::Exception & ex)
        {
            cout << ex.what() << endl;
        }
        catch(std::exception & ex)
        {
            cout << "Error: could not open shared memory, check usage and parameters with help." << endl;
            cout << "detail: " << ex.what() << endl;
            hub = NULL;
        }
    }
    else if(type == "help")
    {
        cout << "open type resource [params]      " << endl;
        cout << "supported types:                 " << endl;
        cout << "                                 " << endl;
        cout << " 1) shm (shared memory)          " << endl;
        cout << "    params: name size modifier   " << endl;
        cout << "    ex: open shm Cloop:OTPs 65 K " << endl;
    }
    else
    {
        cout << "Error: undefined hub type " << type << ". Check help." << endl;
    }
}

void close()
{
    if(hub == NULL)
        cout << "No active hub" << endl;
    else
    {
        delete hub;
        hub = 0;
        cout << "Closed active hub" << endl;
    }
}

void printVarHelp()
{
    cout << "usage: <get/set/check> type name " << endl;
    cout << "supported types:                 " << endl;
    cout << "                                 " << endl;
    cout << " 1) Booleans: bool,b             " << endl;
    cout << " 2) Integers: int,i              " << endl;
    cout << " 3) Floating points:             " << endl;
    cout << "      float,  f                  " << endl;
    cout << "      double, d                  " << endl;
    cout << "      real,   r                  " << endl;
    cout << " 4) Longs: long,l                " << endl;
    cout << " 5) Strings: string,s            " << endl;
}

bool parseType(const std::string & typeStr, VariantType & output)
{
    if(typeStr == "bool" || typeStr == "b")
        output = Boolean;
    else if(typeStr == "int" || typeStr == "i")
        output = Integer;
    else if(typeStr == "long" || typeStr == "l")
        output = Long;
    else if(typeStr == "double" || typeStr == "float" || typeStr == "real" || typeStr == "d" || typeStr == "f" || typeStr == "r")
        output = Double;
    else if(typeStr == "string" || typeStr == "s")
        output = String;
    else
        return false;

    return true;
}

std::string sizeToHuman(long size)
{
    double sz = size;
    int factor = 0;
    while(sz > 1024)
    {
        sz /= 1024;
        factor += 3;
    }

    std::stringstream ss;
    ss << std::setprecision(3) << sz;

    if(factor == 0)
        ss << "b";
    else if(factor == 3)
        ss << "K";
    else if(factor == 6)
        ss << "M";
    else if(factor == 9)
        ss << "G";
    else if(factor == 12)
        ss << "T";
    else
        ss << "??";

    return ss.str();
}

void listVars()
{
    if(!hub)
    {
        cout << "Hub is not open" << endl;
        return;
    }

    std::string filter;
    if(!stream->eof())
        *stream >> filter;
    else
        filter = "*";

    cout << "Listing:" << endl;
    for(Variant varName : hub->list(filter))
        cout  << "  " << varName.toString() << endl;
}

void getVar()
{
    if(!hub)
    {
        cout << "Hub is not open" << endl;
        return;
    }

    std::string typeStr;
    std::string name;
    VariantType type;
    *stream >> typeStr >> name;
    if(!parseType(typeStr, type))
        printVarHelp();
    else
        printVarValue(type, name);
}

void printVarValue(const VariantType & varType, const std::string & varName)
{
    try
    {
        Variant result = hub->read(varType, varName);
        cout << varName << ": " << result.toString() << endl;
    }
    catch (...)
    {
        cout << "Could not get value for " << varName << endl;
    }
}

void setVar()
{
    if(!hub)
    {
        cout << "Hub is not open" << endl;
        return;
    }

    try
    {
        VariantType varType;
        std::string typeStr;
        std::string varName;
        std::string value;
        *stream >> typeStr >> varName >> value;

        if(!parseType(typeStr, varType))
            printVarHelp();
        else
        {
            Variant v = Variant::parse(varType, value);
            hub->write(varType, varName, v);
            printVarValue(varType, varName);
        }
    }
    catch(Exception & ex)
    {
        cout << "Error: " << ex.what() << endl;
    }
    catch(std::exception & ex)
    {
        cout << "Error: " << ex.what() << endl;
    }
}

void checkVar()
{
    if(!hub)
    {
        cout << "Hub is not open" << endl;
        return;
    }

    try
    {
        VariantType varType;
        std::string typeStr;
        std::string varName;
        *stream >> typeStr >> varName;

        if(!parseType(typeStr, varType))
            printVarHelp();
        else
        {
            bool exists = hub->check(varType, varName);
            cout << (exists ? "exists" : "does not exist") << endl;
        }
    }
    catch(Exception & ex)
    {
        cout << "Error: " << ex.what() << endl;
    }
    catch(std::exception & ex)
    {
        cout << "Error: " << ex.what() << endl;
    }
}

void dropVar()
{
    if(!hub)
    {
        cout << "Hub is not open" << endl;
        return;
    }

    try
    {
        VariantType varType;
        std::string typeStr;
        std::string varName;
        *stream >> typeStr >> varName;

        if(!parseType(typeStr, varType))
            printVarHelp();
        else
        {
            bool dropped = false;
            if(!stream->eof())
            {
                std::string varKey;
                *stream >> varKey;
                dropped = hub->dropMapEntry(varType, varName, varKey);
            }
            else
                dropped = hub->drop(varType, varName);
            
            cout << (dropped ? "dropped" : "drop failed") << endl;
        }
    }
    catch(Exception & ex)
    {
        cout << "Error: " << ex.what() << endl;
    }
    catch(std::exception & ex)
    {
        cout << "Error: " << ex.what() << endl;
    }
}
