#include <iostream>
#include <DataHub.h>
#include <string>

#include "Log.h"
using std::cout;
using std::endl;



int main()
{
    using namespace clooponline;
    using namespace std;

    DataHub * dh = DataHub::connectToRedis("127.0.0.1", 6379);
    VariantMap map;
    map.insert(std::make_pair("a", "I"));
    map.insert(std::make_pair("b", "am"));
    map.insert(std::make_pair("c", "black"));
    map.insert(std::make_pair("d", "board !"));

    dh->write(Map, "a", map);
    for(auto pair : dh->read(Map, "a").exportToMap(Integer))
        cout << pair.first << ": " << pair.second.toString() << endl;

/*    dh->write(String, "c", "hello world");
    cout << dh->read(String, "c").toString() << endl;*/

    return 0;
}
