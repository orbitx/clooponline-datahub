# set search hint directories
set(
     hiredis_POSSIBLE_ROOT_PATHS
     $ENV{hiredis_ROOT}
     /usr/local
     /usr
)


# find hiredis include directory
# =================================================================================

find_path(
    hiredis_INCLUDE_DIR
    NAME          hiredis/hiredis.h
    HINTS         ${hiredis_POSSIBLE_ROOT_PATHS}
    PATH_SUFFIXES "include" "deps"
)

if(NOT hiredis_INCLUDE_DIR)
    message(STATUS "Checking for hiredis... no")
    message(FATAL_ERROR "Could not find include path for hiredis, try setting hiredis_ROOT")
endif()


# find hiredis library
# =================================================================================

# library for debug builds
find_library(
    hiredis_LIBRARY_DEBUG
    NAMES          hiredis
    HINTS          ${hiredis_POSSIBLE_ROOT_PATHS}
    PATH_SUFFIXES  "msvs/x64/Debug"
    DOC            "hiredis library for debug builds"
)

# library for release builds
find_library(
    hiredis_LIBRARY_RELEASE
    NAMES          hiredis
    HINTS          ${hiredis_POSSIBLE_ROOT_PATHS}
    PATH_SUFFIXES  "msvs/x64/Release"
    DOC            "hiredis library for release builds"
)

# create library name for linking
set(hiredis_LIBRARY "")
if(hiredis_LIBRARY_DEBUG AND hiredis_LIBRARY_RELEASE)
    set(hiredis_LIBRARY "optimized;${hiredis_LIBRARY_RELEASE};debug;${hiredis_LIBRARY_DEBUG}")
elseif(hiredis_LIBRARY_DEBUG)
    set(hiredis_LIBRARY "${hiredis_LIBRARY_DEBUG}")
elseif(hiredis_LIBRARY_RELEASE)
    set(hiredis_LIBRARY "${hiredis_LIBRARY_RELEASE}")
endif()

# check the result
if(NOT hiredis_LIBRARY)
    message(STATUS "Checking for hiredis... no")
    message(STATUS "hiredis include directory: ${hiredis_INCLUDE_DIR}")
    message(FATAL_ERROR "Could not find hiredis library")
endif()


# find hiredis' interop library
# =================================================================================

find_library(
    hiredis_interop_LIBRARY_DEBUG
    NAMES          Win32_Interop
    HINTS          ${hiredis_POSSIBLE_ROOT_PATHS}
    PATH_SUFFIXES  "msvs/x64/Debug"
    DOC            "Windows modified interop library for debug builds"
)

find_library(
    hiredis_interop_LIBRARY_RELEASE
    NAMES          Win32_Interop
    HINTS          ${hiredis_POSSIBLE_ROOT_PATHS}
    PATH_SUFFIXES  "msvs/x64/Release"
    DOC            "Windows modified interop library for release builds"
)

set(hiredis_interop_LIBRARY "")
if(hiredis_interop_LIBRARY_DEBUG AND hiredis_interop_LIBRARY_RELEASE)
    set(hiredis_interop_LIBRARY "optimized;${hiredis_interop_LIBRARY_RELEASE};debug;${hiredis_interop_LIBRARY_DEBUG}")
elseif(hiredis_interop_LIBRARY_DEBUG)
    set(hiredis_interop_LIBRARY "${hiredis_interop_LIBRARY_DEBUG}")
elseif(hiredis_interop_LIBRARY_RELEASE)
    set(hiredis_interop_LIBRARY "${hiredis_interop_LIBRARY_RELEASE}")
endif()

# check the result
if(NOT hiredis_interop_LIBRARY)
  message(STATUS "Checking for hiredis' interop... no")
  message(STATUS "hiredis include directory: ${hiredis_INCLUDE_DIR}")

    if(WIN32)
        message(FATAL_ERROR "Could not find hiredis interop library")
    else()
        message(STATUS "Could not find hiredis interop library")
    endif()
endif()


# Sum up libraries
# =================================================================================

if(WIN32)
    set(Ws2_32_LIBRARY "optimized;Ws2_32;debug;Ws2_32")
else()
    set(Ws2_32_LIBRARY "")
endif()

set(
    hiredis_LIBRARIES

    ${hiredis_LIBRARY}
    ${hiredis_interop_LIBRARY}
    ${Ws2_32_LIBRARY}
)


# everything is found. just finish up
# =================================================================================

set(hiredis_FOUND TRUE CACHE BOOL "Whether hiredis is found on the system or not")
set(hiredis_INCLUDE_DIR ${hiredis_INCLUDE_DIR} CACHE PATH "hiredis include directory")
set(hiredis_LIBRARIES ${hiredis_LIBRARIES} CACHE FILEPATH "hiredis library for linking against")

message(STATUS "Checking for hiredis... yes")
